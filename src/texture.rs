use plain_renderer::*;

use opengl_graphics::*;
use graphics;
use graphics::Transformed;

pub struct PistonTexture(pub Texture, pub [u64; 2]);

impl Size for PistonTexture {
    fn size(&self) -> [u64; 2] {
        self.1
    }
}

pub struct PistonRefTexture<'a>(pub &'a Texture, pub [u64; 2]);

impl<'a> Size for PistonRefTexture<'a> {
    fn size(&self) -> [u64; 2] {
        self.1
    }
}

/*impl<'a> Render<PistonRenderer<'a>> for Image {
    fn render(&self, area: Area, rect: Rect, renderer: &mut PistonRenderer) {
        let size = max_size(self.min_size(), rect.1);

        let image = graphics::Image::new().rect([0.0, 0.0, size[0], size[1] as f64]);

        image.draw(
            &self.0,
            &area_to_draw_state(area, renderer.size[1]),
            renderer.transform.trans(
                rect.0[0],
                rect.0[1],
            ),
            renderer.graphics,
        );
    }
}

impl<'a, 'b> Render<PistonRenderer<'a>> for RefImage<'b> {
    fn render(&self, area: Area, rect: Rect, renderer: &mut PistonRenderer) {
        let size = max_size(self.min_size(), rect.1);

        let image = graphics::Image::new().rect([0.0, 0.0, size[0], size[1]]);

        image.draw(
            self.0,
            &area_to_draw_state(area, renderer.size[1]),
            renderer.transform.trans(
                rect.0[0],
                rect.0[1],
            ),
            renderer.graphics,
        );
    }
}*/
