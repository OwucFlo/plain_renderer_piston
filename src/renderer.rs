use std::cmp;

use graphics::*;
use graphics::character::*;
use graphics::draw_state::*;
use opengl_graphics::*;
use plain_renderer::*;

use texture::*;
use font::*;

pub struct PistonRenderer<'a> {
    pub graphics: &'a mut GlGraphics,
    pub transform: math::Matrix2d,
    pub size: [u32; 2],
}

impl<'a> Renderer for PistonRenderer<'a> {
    fn clear(&mut self, area: Area, color: Color) {
        self.graphics.use_draw_state(&area_to_draw_state(area, self.size[1]));
        clear(convert_color(color), self.graphics);
    }

    fn polygon(&mut self, area: Area, polygon: &[[i64; 2]], color: Color) {
        unimplemented!()
    }

    fn line(
        &mut self,
        area: Area,
        line: [[i64; 2]; 2],
        color: Color,
        radius: u64,
        shape: LineShape,
    )
    {
        self.graphics.line(
            &Line {
                color: convert_color(color),
                radius: radius as f64,
                shape: convert_line_shape(shape),
            },
            [line[0][0] as f64, line[0][1] as f64, line[1][0] as f64, line[1][1] as f64],
            &area_to_draw_state(area, self.size[1]),
            self.transform
        );
    }

    fn rect(
        &mut self,
        area: Area,
        rect: Rect,
        color: Color,
        shape: RectShape,
        border_color: Color,
        border_radius: u64,
    ) {
        let size = max_size(rect.1, shape.rect_min_size());

        let shape = convert_rect_shape(shape);

        let border = rectangle::Border {
            color: convert_color(border_color),
            radius: border_radius as f64,
        };

        let rectangle = Rectangle {
            color: convert_color(color),
            shape,
            border: Some(border),
        };

        self.graphics.rectangle(
            &rectangle,
            [
                rect.0[0] as f64,
                rect.0[1] as f64,
                size[0] as f64,
                size[1] as f64,
            ],
            &area_to_draw_state(area, self.size[1]),
            self.transform,
        );
    }
}

impl<'a> TextRenderer<PistonFont> for PistonRenderer<'a> {
    fn text(&mut self, area: Area, rect: Rect, font: &mut PistonFont, text: &str, color: Color) {
        let min_size = font.text_size(text);
        let image = Image::new_color(convert_color(color));
        let mut x = rect.0[0] as f64;
        let mut y = rect.0[1] as f64 + min_size[1] as f64;
        for ch in text.chars() {
            if let Ok(character) = font.0.character(32, ch) {
                let ch_x = x + character.left();
                let ch_y = y - character.top();

                image.draw(
                    character.texture,
                    &area_to_draw_state(area, self.size[1]),
                    self.transform.trans(ch_x, ch_y),
                    self.graphics,
                );
                x += character.width();
                y += character.height();
            }
        }
    }
}

impl<'a> TextureRenderer<PistonTexture> for PistonRenderer<'a> {
    fn texture(&mut self, area: Area, rect: Rect, texture: &PistonTexture) {
        let size = max_size(texture.size(), rect.1);

        let image = Image::new().rect(
            [
                rect.0[0] as f64,
                rect.0[1] as f64,
                rect.1[0] as f64,
                rect.1[1] as f64,
            ]
        ).color([1.0, 1.0, 1.0, 1.0]);

        image.draw(
            &texture.0,
            &area_to_draw_state(area, self.size[1]),
            self.transform,
            self.graphics,
        );
    }
}

impl<'a, 'b> TextureRenderer<PistonRefTexture<'a>> for PistonRenderer<'b> {
    fn texture(&mut self, area: Area, rect: Rect, texture: &PistonRefTexture) {
        let size = max_size(texture.size(), rect.1);

        let image = Image::new().rect(
            [
                rect.0[0] as f64,
                rect.0[1] as f64,
                rect.1[0] as f64,
                rect.1[1] as f64,
            ]
        ).color([1.0, 1.0, 1.0, 1.0]);

        image.draw(
            texture.0,
            &area_to_draw_state(area, self.size[1]),
            self.transform,
            self.graphics,
        );
    }
}

pub fn convert_color(color: [u8; 4]) -> [f32; 4] {
    [
        color[0] as f32 / 255.0,
        color[1] as f32 / 255.0,
        color[2] as f32 / 255.0,
        color[3] as f32 / 255.0,
    ]
}

pub fn area_to_draw_state(area: Area, height: u32) -> DrawState {
    DrawState {
        scissor: if let Some(area) = area {
            Some(
                [
                    area.0[0] as u32,
                    height - cmp::min((area.0[1] + area.1[1] as i64) as u32, height),
                    area.1[0] as u32,
                    area.1[1] as u32
                ]
            )
        } else { None },
        stencil: None,
        blend: Some(Blend::Alpha),
    }
}

pub fn convert_line_shape(shape: LineShape) -> line::Shape {
    match shape {
        LineShape::Square => line::Shape::Square,
        LineShape::Round => line::Shape::Round,
    }
}

pub fn convert_rect_shape(shape: RectShape) -> rectangle::Shape {
    match shape {
        RectShape::Square => rectangle::Shape::Square,
        RectShape::Round(radius) => rectangle::Shape::Round(radius as f64, 16),
    }
}
