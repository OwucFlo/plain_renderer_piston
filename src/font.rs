use plain_renderer::*;
use graphics::*;
use graphics::character::CharacterCache;
use opengl_graphics::*;


pub struct PistonFont(pub GlyphCache<'static>);

impl TextSize for PistonFont {
    fn text_size(&mut self, text: &str) -> [u64; 2] {
        let mut x = 0.0;
        let mut y = 0.0;

        for ch in text.chars() {
            if let Ok(character) = self.0.character(32, ch) {
                x += character.width();
                let height = character.texture.get_height() as f64;
                if height > y {
                    y = height;
                }
            }
        }
        [x as u64, y as u64]
    }
}

//impl<'a> FontRender<PistonRenderer<'a>> for PistonFont {
//    fn render_text(
//        &mut self,
//        text: &str,
//        area: Area,
//        rect: Rect,
//        color: Color,
//        renderer: &mut PistonRenderer
//    )
//    {
//        let min_size = self.text_min_size(text);
//        let image = Image::new_color(convert_color(color));
//        let mut x = rect.0[0];
//        let mut y = rect.0[1] + min_size[1];
//        for ch in text.chars() {
//            if let Ok(character) = self.0.character(32, ch) {
//                let ch_x = x + character.left();
//                let ch_y = y - character.top();
//                image.draw(
//                    character.texture,
//                    &area_to_draw_state(area, renderer.size[1]),
//                    renderer.transform.trans(ch_x, ch_y),
//                    renderer.graphics,
//                );
//                x += character.width();
//                y += character.height();
//            }
//        }
//    }
//}
